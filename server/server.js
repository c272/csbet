//Adding all dependencies.
const pug = require('pug');
const express = require('express');
const fileExists = require('file-exists');
const jsonfile = require('jsonfile');
const session = require('express-session');
const steamOpenID = require('steam-login');
const bodyParser = require('body-parser');
const editJSON = require('edit-json-file');
const outer = this;
var app = express();
const fs = require('fs');
outer.config = require("./config/config.json");
outer.id = require('./config/id.json');

//Spinning up socketio server.
var ioserver = require('http').createServer(function(req, res) {
    res.writeHead(200);
    res.end("Successful connection, sockets enabled.");
});

//Quickfire server variables.
server = app.listen(80);
const io = require('socket.io')(server);

//Listening on port 80.
console.log("IOSERVER: Listening on port 80.");

//Configuring HTTP server to run on PUG (TEMPORARY!)
app.set('views', __dirname+'/views');
app.set('view engine', 'pug');

//Telling Express to use a new session with a secret key.
//WARNING:
//KEEP THE SECRET KEY COMPLETELY UNTOUCHED. CHANGING IT WILL BREAK THE ENTIRE SITE.
app.use(session({
    secret: "a7W9VGDJp3P4TZeU6?7djDa@*95kcyRj",
    resave: false,
    saveUninitialized: false
}));

//Telling Express to use Steam OpenID logon.
app.use(steamOpenID.middleware({
    realm: "http://localhost:3000",
    verify: "http://localhost:3000/verify",
    apiKey: "B10AA6BE0488AAA1E3D6FDC90A416D67"
}));
console.log("SERVER: Steam OpenID connected.");

//Telling Express to use body parser.
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// SECTION 1
// HTTP REQUESTS
// All webpage serves below.

// On request, responding with required action.
app.all('*', function (req, res, next) {
    //Header functions here.
    next();
});

//When a user asks to log in, authenticate with steamOpenID.
app.get('/login', steamOpenID.authenticate(), function(req, res) {
    console.log("SERVER: New login request, redirecting.");
    res.redirect("/");
});

//Verifying Steam login after OpenID.
app.get('/verify', steamOpenID.verify(), function(req, res) {
    //Put checks here.
    //Redirecting back to homepage.
    res.redirect("/");
});

//When a user requests to log out.
app.get('/logout', steamOpenID.enforceLogin("/"), function(req, res) {
    req.logout();
    //Back to homepage.
    res.redirect("/");
});

//Showing index when base URL is loaded.
app.get('/', function(req, res){

    //Getting current matches to put in page.
    var activeMatches = getCurrentMatches();

    //Checking if logged in.
    if (req.user) {
        //If they are, render "Hello" message, and post matches.
        res.render('index', {
            user: "Hello, "+req.user.username,
            logUrl: "/logout",
            logText: "Log Out",
            mArray: activeMatches
        });
    } else {
        //If they aren't, render "You are not logged in", and post matches.
        res.render('index', {
            user: "You are not logged in.",
            logUrl: "/login",
            logText: "Log In",
            mArray: activeMatches
        });
    }
});

//Upon a betting page being requested:
app.get('/bet', function(req, res){
    //Checking for queries.
    if (req.query.id) {
        //Setting filepath.
        var file = "./matches/"+req.query.id+".json";

        //Checking if file with that ID exists.
        if (fileExists.sync(file)) {
            //Checking if incoming user is admin.
            if (req.user) {
                if (checkIfAdmin(req.user.steamid)) {
                    var isAdmin = true;
                }  
            }

            //Checking if betfile exists.
            var file2 = "./matches/bets/"+req.query.id+".json";
            if (fileExists.sync(file2)) {
                //Reading JSON.
                var matchJSON = jsonfile.readFileSync(file);
                //Rendering with appropriate variables.
                res.render('bet', {
                    team1: matchJSON.team1,
                    team2: matchJSON.team2,
                    odds1: matchJSON.odds1,
                    odds2: matchJSON.odds2,
                    maxbet: matchJSON.maxbet,
                    matchID: req.query.id,
                    admin: isAdmin
                });
            } else {
                //Redirect 404, match is already closed.
                res.redirect('404');
            }
        } else {
            //Rendering 404, match not found.
            res.render('404.pug');
        }

    } else {
        //Rendering 404, no match given.
        res.render('404.pug');
    }
});

//Upon a watch page being requested:
app.get('/watch', function(req, res){
    //Checking for queries.
    if (req.query.id) {
        //Setting filepath.
        var file = "./matches/"+req.query.id+".json";

        //Checking if file with that ID exists.
        if (fileExists.sync(file)) {
            //Reading JSON.
            var matchJSON = jsonfile.readFileSync(file);
            //Rendering with appropriate variables.
            res.render('watch', {
                team1: matchJSON.team1,
                team2: matchJSON.team2,
                odds1: matchJSON.odds1,
                odds2: matchJSON.odds2,
                maxbet: matchJSON.maxbet,
                matchID: '/bet?id='+req.query.id
            });
        } else {
            //Rendering 404.
            res.render('404.pug');
        }

    } else {
        //Rendering 404, match not found.
        res.render('404.pug');
    }
});

//Serving admin console.
app.get('/admin', function(req, res) {
    //Checking if user is logged in.
    if (req.user) {
        if (checkIfAdmin(req.user.steamid)) {
            //We found them.
            foundAdmin = true;
            //Getting news, matches then rendering.
            var news = getLatestNews();
            var matches = getCurrentMatches();

            //Render.
            res.render('admin', {
                user: req.user.username,
                array: news,
                mArray: matches
            });
        }

        //Verifying admin.
        if (foundAdmin==false) {
            //Redirecting to "Unauthorised".
            res.redirect('401');
        }
    } else {
        //Redirecting to "Unauthorised".
        res.redirect('401');
    }
});

//When user is not authorised.
app.get('/401', function(req, res) {
    res.render('401');
})

//When withdraw page is called.
app.get('/withdraw', function(req, res) {
    //Checking user is logged in.
    if (!req.user) {
        //If no user cookies, redirect 401.
        res.redirect('401');
    } else {
        //Setting file location.
        var file = "./users/"+req.user.steamid+".json";

        //Checking if file exists.
        if (fileExists.sync(file)) {
            //Reading.
            var clientData = jsonfile.readFileSync(file);
            var balance = clientData.balance;

            //Rendering page.
            res.render('withdraw', {
                balance: balance
            });
        } else {
            //If no, create one with no balance.
            jsonfile.writeFileSync(file, {balance: 0});
            var balance = 0;

            //Rendering page.
            res.render('withdraw', {
                balance: balance
            });
        }
    }
});

//When an error page is thrown.
app.get('/error', function(req, res) {
    if(req.query.flag) {
        //Checking which error's been thrown.
        if (req.query.flag=="nobot") {
            res.render('error', {
                error: "No bots are currently available to withdraw."
            });
        } else if (req.query.flag=="bal") {
            res.render('error', {
                error: "Amount too high."
            });
        } else if (req.query.flag=="badid") {
            res.render('error', {
                error: "Trade URL invalid."
            });
        } else if (req.query.flag=="badamount") {
            res.render('error', {
                error: "Invalid withdraw amount."
            });
        } else {
            //Error ID not found.
            res.redirect('404');
        }
    } else {
        //Page isn't found.
        res.redirect('404');
    }
});

//When the news page is requested.
app.get('/news', function(req, res) {
    if (req.query.id) {
        //Checking if that news ID exists.
        file = "./news/"+req.query.id+".json";
        if (fileExists.sync(file)) {
            //Reading the post's JSON file.
            var newsData = jsonfile.readFileSync(file);
            //Render the page with news on.
            res.render('news_post', {
                title: newsData.title,
                body: newsData.body,
                author: newsData.author
            });
        }
    } else {
        //Loading all news.
        var news = getLatestNews();
        res.render('news', {
            array: news
        });
    }
});

//On settings request.
app.get('/settings', function(req, res) {
    //Checking if logged in.
    if (req.user) {
        //Rendering.
        res.render('settings', {
            name: req.user.username
        });
    } else {
        //401.
        res.redirect('401');
    }
});

//When the support page is called.
app.get('/support', function(req, res) {
    //Checking if query.
    if (req.query.id) {
        //Validating query.
        if (fileExists.sync("./support/"+req.query.id+".json") && fileExists.sync("./support/"+req.query.id+".chat.json")) {
            //Reading JSON.
            var ticketData = jsonfile.readFileSync("./support/"+req.query.id+".json");
            //Reading chat JSON.
            var chatData = jsonfile.readFileSync("./support/"+req.query.id+".chat.json");

            //Rendering page.
            res.render('supportTicket', {
                name: ticketData.name,
                description: ticketData.description,
                chat: chatData,
                id: req.query.id
            });
        } else {
            //Ticket not found :(
            res.redirect('404');
        }
    } else {
        //No query, so dashboard.
        //Checking if logged in.
        if (req.user) {
            //Grabbing user data.
            var userDataPath = "./users/"+req.user.steamid+".json";
            if (fileExists.sync(userDataPath)) {
                var userData = jsonfile.readFileSync(userDataPath);
                var supportArray = userData.support;

                //Grabbing ticket data.
                var tickets = grabTicketData(supportArray);
                //Rendering site with ticket data.
                if (tickets!=[]) {
                    //Passing to PUG.
                    res.render('supportDashboard', {
                        name: req.user.username,
                        tickets: tickets
                    });
                } else {
                    //No tickets, blank dashboard.
                    res.render('supportDashboard', {
                        name: req.user.username,
                        tickets: false
                    });
                }
            } else {
                //No user data? Blank dashboard.
                res.render('supportDashboard', {
                    name: req.user.username,
                    tickets: false
                });
            }
        } else {
            //Prompting login.
            res.render('supportLogin');
        }
    }
});

//On new match form being submitted.
app.post('/newmatch', function(req, res) {
    //Checking if admin.
    var isAdmin = false;
    if (req.user) {
        isAdmin = checkIfAdmin(req.user.steamid);
    }

    //Checking required parameters.
    if (isAdmin && req.body.team1 && req.body.team2 && req.body.odds1 && req.body.odds2 && req.body.maxbet) {
        //Readying new match file.
        var matchObj = {
            team1: req.body.team1, 
            team2: req.body.team2,
            odds1: req.body.odds1,
            odds2: req.body.odds2,
            maxbet: req.body.maxbet,
        };
        
        //Setting file name.
        var id = outer.id.num+1;
        var matchFile = "./matches/"+id+".json";

        //Adding 1 to current ID number.
        var idFile = "./config/id.json";
        var idObj = {num: outer.id.num+1};
        jsonfile.writeFileSync(idFile, idObj);
        //Reading back in file.
        outer.id = jsonfile.readFileSync(idFile);

        //Writing match to file.
        jsonfile.writeFileSync(matchFile, matchObj);

        //Opening match array, pushing.
        var maFile = "./matches/_array.json";
        var matchArray = jsonfile.readFileSync(maFile);
        matchArray.push(id);

        //Writing array back to file.
        jsonfile.writeFileSync(maFile, matchArray);

        //Creating blank bets file for user bets.
        var bets = "./matches/bets/"+id+".json";
        jsonfile.writeFileSync(bets, []);

        //Redirecting user back to admin panel.
        res.redirect("/admin");
    } else {
        if (isAdmin) {
            //404.
            res.redirect('404');            
        } else {
            //401.
            res.redirect('401');
        }
    }
});

//When a user wants to withdraw some skins.
app.post('/userWithdraw', function(req, res) {
    if (!req.user) {
        //If no user cookies, redirect 401.
        res.redirect('401');
    } else {
        //Grabbing user balance.
        var file = "./users/"+req.user.steamid+".json";

        //Checking if file exists.
        if (fileExists.sync(file)) {
            //Reading.
            var clientData = jsonfile.readFileSync(file);
        } else {
            //If no, create one with no balance.
            jsonfile.writeFileSync(file, {balance: 0});
            //Reading.
            var clientData = jsonfile.readFileSync(file);
        }

        //Extracting balance from file.
        var balance = clientData.balance;

        //Validating request.
        if (req.body.amount<=balance && req.body.amount>0 && !isNaN(req.body.amount)) {
            //Converting balance to USD.
            var amount = coinsToUSD(req.body.amount);

            //Sending request to bot to extract to that user.
            worked = sendTradeRequest(amount, req.user.steamid);
            if (worked==true) {
                //Taking off balance.
                clientData.balance = balance-req.body.amount;
                jsonfile.writeFileSync(file, clientData);

                //Back to the home page!
                res.redirect("/");
            } else if (worked=="noid") {
                //Go to settings, trade URL has not been put in.
                res.redirect("/settings?flag=nourl");
            } else if (worked=="nobot") {
                //Back to withdraw, no bots are available.
                res.redirect("/error?flag=nobot");
            }
        } else {
            //Showing user "invalid" flag.
            res.redirect('/error?flag=badamount');
        }
    }
});

app.post('/newpost', function(req, res) {
    //Checking if admin.
    var isAdmin = false;
    if (req.user) {
        isAdmin = checkIfAdmin(req.user.steamid);
    }

    //Checking required parameters.
    if (req.body.body && req.body.title && req.body.author && isAdmin) {
        //Writing to post JSON using ID.
        var id = jsonfile.readFileSync("./news/id.json");
        var currentID = id.id;

        //Adding 1 to ID.
        id.id = currentID+1;
        jsonfile.writeFileSync("./news/id.json", id);

        //Creating post.
        var file = "./news/"+currentID+".json";
        var description = req.body.body.substring(0, 150)+"...";
        var obj = {title: req.body.title, body: req.body.body, author: req.body.author, description: description};
        jsonfile.writeFileSync(file, obj);

        //Pushing to array.
        var postArray = jsonfile.readFileSync("./news/_array.json");
        postArray.push(currentID);
        jsonfile.writeFileSync("./news/_array.json", postArray);

        //Back to admin console.
        res.redirect("/admin");
    } else {
        if (isAdmin) {
            //404.
            res.redirect('404');            
        } else {
            //401.
            res.redirect('401');
        }
    }
});

//When a delete match request is issued.
app.get('/deleteMatch', function(req, res) {
    //Checking if admin.
    var isAdmin = false;
    if (req.user) {
        isAdmin = checkIfAdmin(req.user.steamid);
    }

    if (req.query.id && isAdmin) {
        //Setting ID.
        var id = req.query.id;

        //Finding the match JSON.
        var file = "./matches/"+id+".json";
        var file2 = "./matches/bets/"+id+".json";
        if (fileExists.sync(file)) {
            //"Unlink", AKA Delete.
            fs.unlink(file);
        }
        if (fileExists.sync(file2)) {
            //Deleting bets file.
            fs.unlink(file2);
        }

        //Removing from match array.
        var matches = jsonfile.readFileSync("./matches/_array.json");
        for (i in matches) {
            if (matches[i]==id) {
                //Goodbye, match.
                matches.splice(i, 1);
            }
        }

        //Writing back array file.
        jsonfile.writeFileSync("./matches/_array.json", matches);

        //Redirecting to admin panel.
        res.redirect('admin');
    } else {
        if (isAdmin) {
            //404.
            res.redirect('404');            
        } else {
            //401.
            res.redirect('401');
        }
    }
});

//When a delete match request is issued.
app.get('/deletePost', function(req, res) {
    //Checking if admin.
    var isAdmin = false;
    if (req.user) {
        isAdmin = checkIfAdmin(req.user.steamid);
    }

    if (req.query.id && isAdmin) {
        //Setting ID.
        var id = req.query.id;

        //Finding the match JSON.
        var file = "./news/"+id+".json";
        if (fileExists.sync(file)) {
            //"Unlink", AKA Delete.
            fs.unlink(file);
        }

        //Removing from match array.
        var news = jsonfile.readFileSync("./news/_array.json");
        for (i in news) {
            if (news[i]==id) {
                //Goodbye, match.
                news.splice(i, 1);
            }
        }

        //Writing back array file.
        jsonfile.writeFileSync("./news/_array.json", news);

        //Redirecting to admin panel.
        res.redirect('admin');
    } else {
        if (isAdmin) {
            //404.
            res.redirect('404');            
        } else {
            //401.
            res.redirect('401');
        }
    }
});

//When a match is signed as finished by admin.
app.get("/closeMatch", function(req, res) {
    //Checking if admin.
    var isAdmin = false;
    if (req.user) {
        isAdmin = checkIfAdmin(req.user.steamid);
    }

    //Checking for correct parameters.
    if (req.query.id && req.query.winner && isAdmin) {
        //Loading match array and archiving.
        var matches = jsonfile.readFileSync("./matches/_array.json");
        for (i in matches) {
            if (matches[i]==req.query.id) {
                //Goodbye match.
                matches.splice(i, 1);
            }
        }
        
        //Writing back to JSON.
        jsonfile.writeFileSync('./matches/_array.json', matches);

        //Changing balances for lose/win.
        var winner = req.query.winner;
        var bets = jsonfile.readFileSync("./matches/bets/"+req.query.id+".json");
        var match = jsonfile.readFileSync("./matches/"+req.query.id+".json");

        for (i in bets) {
            if (bets[i].team==winner) {
                //Finding user file.
                if (fileExists.sync("./users/"+bets[i].user+".json")) {
                    var userData = jsonfile.readFileSync("./users/"+bets[i].user+".json");
                } else {
                    jsonfile.writeFileSync("./users/"+bets[i].user+".json", {balance: 0});
                    userData = {balance: 0};
                }
                //Adding money to account.
                if (winner=1) {
                    userData.balance += bets[i].amount*match.odds1;
                    jsonfile.writeFileSync("./users/"+bets[i].user+".json", userData);
                } else {
                    userData.balance += bets[i].amount*match.odds2;
                    jsonfile.writeFileSync("./users/"+bets[i].user+".json", userData);
                }
            }
        }

        //Deleting the bets file.
        var file2 = "./matches/bets/"+req.query.id+".json";
        if (fileExists.sync(file2)) {
            //Deleting bets file.
            fs.unlink(file2);
        }

        //Redirecting to home page.
        res.redirect("/");
    } else {
        if (isAdmin) {
            //404.
            res.redirect('404');            
        } else {
            //401.
            res.redirect('401');
        }
    }
});

//On a bet request.
app.post('/betMatch', function(req, res) {
    //Verifying all correct variables are present.
    if (req.query.id && req.user && req.body.amount && req.body.team) {
        //Checking for ID.
        var betFile = "./matches/bets/"+req.query.id+".json";
        if (fileExists.sync(file)) {
            //Finding user file.
            if (fileExists.sync("./users/"+req.user.steamid+".json")) {
                var userData = jsonfile.readFileSync("./users/"+req.user.steamid+".json");
            } else {
                jsonfile.writeFileSync("./users/"+req.user.steamid+".json", {balance: 0});
                userData = {balance: 0};
            }

            if (userData.balance<req.body.amount) {
                res.redirect("/error?flag=bal");
            } else {
                //Reading file, adding bet to bet file.
                var match = jsonfile.readFileSync(betFile);
                match.push({
                    user: req.user.steamid,
                    amount: req.body.amount,
                    team: req.body.team
                });

                //Saving file back.
                jsonfile.writeFileSync(betFile, match);

                //Taking balance off user.
                userData.balance = userData.balance-req.body.amount;
                jsonfile.writeFileSync("./users/"+req.user.steamid+".json", userData);

                //Process finished, redirect to watch page.
                res.redirect("/watch?id="+req.query.id);
            }
        } else {
            //404, not found.
            res.redirect('404');
        }
    } else {
        //No user, 401.
        if (!req.user) {
            res.redirect('401');
        } else {
            //404, not found.
            res.redirect('404');
        }
    }
});

//On a trade URL post.
app.post('/tradeUrlSet', function(req, res) {
    //Checking for legit URL.
    if (req.body.url && req.user) {
        //Verifying URL.
        if (req.body.url.substring(0, 42)=="https://steamcommunity.com/tradeoffer/new/") {
            //Saving to file.
            var file = "./users/"+req.user.steamid+".json";
            if (fileExists.sync(file)) {
                //Writing into clientdata, repushing.
                var clientData = jsonfile.readFileSync(file);
                clientData.tradeurl = req.body.url;
                jsonfile.writeFileSync(file, clientData);
            } else {
                //Create a new file, push URL.
                var obj = {tradeurl: req.body.url};
                jsonfile.writeFileSync(file, obj);
            }

            //Redirecting.
            res.redirect('/settings');
        } else {
            //Invalid trade URL, redirect error.
            res.redirect('/error?flag=badid');
        }
        //Saving to user file.

    } else {
        res.redirect('401');
    }
});

app.post('/newTicket', function(req, res) {
    if (req.user && req.body.name && req.body.description) {
        //Creating JSON object.
        var obj = {
            name: req.body.name, 
            description: req.body.description, 
            user: req.user.steamid
        }

        //Loading ID JSON.
        var idfile = jsonfile.readFileSync("./support/_id.json");
        var id = idfile.id+1;

        //Writing JSON file.
        var file = "./support/"+id+".json";
        jsonfile.writeFileSync(file, obj);

        //Writing blank chat file.
        jsonfile.writeFileSync("./support/"+id+".chat.json", []);

        //Writing ID file.
        idfile.id = id;
        jsonfile.writeFileSync("./support/_id.json", idfile);

        //Assigning support ticket to user.
        var userDataPath = "./users/"+req.user.steamid+".json";
        if (fileExists.sync(userDataPath)) {
            //Reading current JSON.
            var userData = jsonfile.readFileSync(userDataPath);

            //Finding existing tickets.
            if (userData.support) {
                //Reading current if existant.
                supportArray = userData.support;
            } else {
                //Creating new array.
                supportArray = [];
            }

            //Pushing new ID to support array.
            supportArray.push(id);

            //Saving.
            userData.support = supportArray;
            jsonfile.writeFileSync(userDataPath, userData);
        } else {
            //If user data doesn't already exist, formulate.
            var obj = {support: [id]};
            //Saving.
            jsonfile.writeFileSync(userDataPath, obj);
        }

        //Redirecting to support panel.
        res.redirect('/support');
    } else {
        //Incorrect POST data.
        res.redirect('401');
    }
});

//When a user attempts to post to ticket chat.
app.post('/ticketChat', function(req, res) {
    if (req.user && req.query.id && req.body.message) {
        //Getting ticket data.
        var ticketData = grabTicketData([req.query.id]);

        //Checking if ticket exists.
        if (ticketData==[]) {
            //Not found, 404.
            res.redirect('404');
        } else {
            //Checking whether user created ticket, or is admin.
            if (ticketData[0].user==req.user.steamid || checkIfAdmin(req.user.steamid)) {
                //Writing to chat.
                var chat = ticketData[0].chat;

                //Checking for null.
                if (chat.length!=0) {
                    chat.push({name: req.user.username, message: req.body.message});
                } else {
                    chat = [{name: req.user.username, message: req.body.message}];
                }

                //Writing back to file.
                jsonfile.writeFileSync("./support/"+req.query.id+".chat.json", chat);

                //Redirecting back to support page.
                res.redirect('/support?id='+req.query.id);
            } else {
                //Not authorised.
                res.redirect('401');
            }
        }
    } else {
        res.redirect('401');
    }
});

//Make sure this is the LAST route.
app.get('*', function(req, res) {
    res.render('404');
});

//All routes defined, listening for any activity on port 3000.
app.listen(3000, () => console.log('SERVER: Listening on port 3000.'));

// SECTION 2
// SOCKET.IO SERVER LISTENERS

//Initialising client array.
var clients = [];

io.sockets.on('connection', function(socket) {
    //When an API key is received.
    socket.on('keyPush', function(data) {
        //Checking key is the same, if it is, exec.
        if (data.key==outer.config.key) {
            
            //Emit success to user.
            console.log("SERVER: User connected, ID "+socket.id);
            socket.emit('keyPushSuccess', true);

            //Pushing socket to client array.
            clients.push({id: socket.id});

            //ALL SOCKET FUNCTIONS HERE!
            socket.on('trade', function(data) {
                //example function
            });

            //When a client disconnects from the server.
            socket.on('disconnect', function() {
                //Validation variable.
                var foundUser = false;
                //Finding client...
                for (i in clients) {
                    if (clients[i].id==socket.id) {
                        clients = clients.slice(i+1);
                        console.log("SERVER: Client "+socket.id+" disconnected.");
                        foundUser = true;
                    }
                }

                //Validating.
                if (foundUser==false) {
                    console.log("SERVER: Error finding client upon disconnect.");
                }
            });

            socket.on('tradeUnfinished', function(data) {
                //Validation variable.
                var currentIndex = null;

                //Finding sending socket.
                for (i in clients) {
                    if (clients[i].id==socket.id) {
                        var currentIndex = i;
                    }
                }

                if (currentIndex!=null) {
                    //Emitting second trade request.
                    var socketid = clients[currentIndex+1].id;
                    io.to(socketid).emit('sendTrade', {steamid: data.id, tradeurl: data.tradeurl, amount: data.amount}); 
                } else {
                    //Failure! Tell the user (somehow).
                    //LATER
                }
            });
        } else {
            //Eject them from the server if failed.
            console.log("SERVER: Client connected with incorrect key. Ejecting...")
            socket.disconnect();
        }
    })
});

// SECTION 3
// EXTERNAL FUNCTIONS
// Place all external functions here.

function getCurrentMatches() {
    //Reading match array.
    var maFile = "./matches/_array.json";
    var matchArray = jsonfile.readFileSync(maFile);
    
    //Creating blank array.
    var currentMatches = [];

    //Getting match properties.
    for (i in matchArray) {
        //Temporary file creation.
        var matchLoc = "./matches/"+matchArray[i]+".json";
        var temp = jsonfile.readFileSync(matchLoc);

        //Getting properties of the match, pushing to array.
        currentMatches.push({name: temp.team1+" VS "+temp.team2, id: matchArray[i]});
    }

    //Returning array.
    return currentMatches;
}

function sendTradeRequest(amount, id) {
    //Sending to client 1, for most of trade.
    for (i in clients) {
        var socketid = clients[0].id
    }

    //Checking if any sockets are connected.
    if (!socketid) {
        return "nobot";
    } else {
        //Finding the trade ticket for that SteamID.
        var file = "./users/"+id+".json";
        if (fileExists.sync(file)) {
            var clientData = jsonfile.readFileSync(file);
        } else {
            //Invalidate.
            var clientData = [];
        }

        //Validation URL.
        var foundUrl = false;
        for (i in clientData) {
            if (clientData.tradeurl) {
                var tradeurl = clientData.tradeurl;
                foundUrl = true;
            }
        }

        //Validating.
        if (foundUrl==true) {
            //Emitting.
            io.to(socketid).emit('sendTrade', {steamid: id, tradeurl: tradeurl, amount: amount}); 
            //Returning true, trade worked.
            return true;
        } else {
            //Failure, redirecting to input trade URL page.
            return "noticket";
        }
    }
}

//Converting coins into USD.
function coinsToUSD(amt) {
    amt = amt/100;
    return amt;
}

//Converting USD to coins.
function USDToCoins(amt) {
    amt = amt*100;
    return amt;
}

//Getting latest news.
function getLatestNews() {
    //Reading news file.
    newsArray = jsonfile.readFileSync("./news/_array.json");

    //Getting the news.
    var news = [];

    for (i in newsArray) {
        //Grabbing post.
        var post = jsonfile.readFileSync("./news/"+newsArray[i]+".json");
        //Pushing to news array.
        news.push({title: post.title, description: post.description, author: post.author, id: newsArray[i]});
    }

    //Returning.
    return news;
}

//Verifies whether a user is an admin.
function checkIfAdmin(userID) {
    //Loading list of admins.
    file = "./config/admins.json";
    adminData = jsonfile.readFileSync(file);

    //Checking if user is admin.
    var foundAdmin = false;
    for (i in adminData) {
        if (userID==adminData[i]) {
            //We found them.
            foundAdmin = true;
            return true;
        }
    }

    //Verifying admin.
    if (foundAdmin==false) {
        return false;
    }
}

//Gets data from an array of ticket IDs.
function grabTicketData(array) {
    //Creating a blank array for return data.
    var tickets = [];

    //Looping through ID array.
    for (i in array) {
        //Checking file.
        var file = "./support/"+array[i]+".json";
        var file2 = "./support/"+array[i]+".chat.json";
        if (fileExists.sync(file) && fileExists.sync(file2)) {
            //If exists, scrape data.
            var ticketFile = jsonfile.readFileSync(file);
            var chatFile = jsonfile.readFileSync(file2);
            var obj = {name: ticketFile.name, id: array[i], user: ticketFile.user, chat: chatFile};

            //Pushing to return array.
            tickets.push(obj);
        }
    }

    //Returning ticket data array.
    return tickets;
}