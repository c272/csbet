//Adding dependencies.
var outer = this;
const io = require('socket.io-client');
const colors = require('colors');
outer.config = require('./config.json');
outer.pricesheet = require('./pricesheet.json');
const SteamUser = require('steam-user');
const SteamTotp = require('steam-totp');
const SteamCommunity = require('steamcommunity');
const TradeOfferManager = require('steam-tradeoffer-manager');

// SECTION 1
// STEAM BOT LOGIC
// Place all Steam jargon here.

const client = new SteamUser();
outer.steamconfig = require('./steam_config.json')
const community = new SteamCommunity();
const manager = new TradeOfferManager({
		steam: client,
		community: community,
		language: 'en',
});

//Setting variables for logon. See "steam_config.json".
const logOnOptions = {
	accountName: outer.steamconfig.username,
	password: outer.steamconfig.password,
	twoFactorCode: SteamTotp.generateAuthCode(outer.steamconfig.sharedsecret),
	rememberPassword: true
};

//Logging on.
client.logOn(logOnOptions);

//Setting persona as "Online", logging to console.
client.on('loggedOn', () =>{
	console.log(colors.green("STEAM: Logged On"));
	client.setPersona(SteamUser.Steam.EPersonaState.Online);
});

//When the session expires, "Relog" back on to Steam.
community.on("sessionExpired", function() {
	client.relog();
	client.webLogOn(logOnOptions);
	const manager = new TradeOfferManager({
		steam: client,
		community: community,
		language: 'en'
	});
	console.log("STEAM: Refreshed web & Steam session.")
});

//Sets new cookies for bot, checks that identity_secret is legitimate.
//Checks for outgoing trade confirmations every 10 seconds, and confirms them.
client.on('webSession', (sessionid, cookies) => {
	manager.setCookies(cookies);
  
	community.setCookies(cookies);
	community.startConfirmationChecker(10000, outer.steamconfig.identitysecret);
});

//When a new offer comes from the owner, accept every time.
//When a donation comes, accept.
//Other offers are processed.
manager.on('newOffer', offer => {
	if (offer.partner.getSteamID64() === outer.steamconfig.owner64) {
		acceptOffer(offer);
	} else if (offer.itemsToGive.length === 0) {
		acceptOffer(offer);
	} else {
		declineOffer(offer);
	}
});

//Accept offer function.
function acceptOffer(offer) {
	offer.accept((err, status) => {
		if (err) {
			console.log("Error: "+err);
		} else {
			console.log('Offer accepted. Status: '+status);
		}
	});
}

//Decline offer function.
function declineOffer(offer) {
	offer.decline(err => {
		if (err) {
			console.log("Error: "+err);
		} else {
			console.log("BOT: Offer declined, invalid trade.");
		}
	});
}

// SECTION 2
// SOCKET.IO 
// Place all listeners here.

//Connecting to server.
console.log("BOT: Connecting to master trade server...");
var socket = io.connect('http://127.0.0.1:80', {reconnect: true});

// ALL LISTENERS HERE!
// SOCKET.ON REQUIRED!

//Complete actions upon server contact:
socket.on('connect', function() {
	console.log("BOT: Connection established.");
    //Sending the validation key to register with server.
    socket.emit("keyPush", 
    {
        key: outer.config.key,
    });
	console.log("BOT: Sending validation key to server...");
 });

 //When our key push works properly.
 socket.on('keyPushSuccess', function() {
    console.log(colors.green("BOT: Successful keypush to server. Authorised."));
 });

 //On trade request.
 socket.on('sendTrade', function(data) {
	//Setting initial vars.
	var pricesheet = outer.pricesheet;
	var id = data.steamid;
	var url = data.tradeurl;
	var amount = data.amount;
	var currentTotal = 0;
	var goodOffer = false;
	console.log("SET VARS");

	//Creating trade.
	var offer = manager.createOffer(url);
	console.log("CREATED TRADE");
	//Grabbing own inventory.
	community.getUserInventoryContents(outer.steamconfig.bot64, 730, 2, true, function(err, myInventory, currency, totalItems) {
		console.log("RECEIVED INVENTORY");
		//Adding items to trade.
		for (i in myInventory) {
			if (amount-outer.config.variance <= currentTotal) {
				//Do nothing, but set "goodOffer" to true.
				console.log("OFFER VALIDATED");
				goodOffer = true;
			} else {
				//Setting market name.
				var item = myInventory[i].market_name;
				console.log("ADDING ITEM "+item);

				//Finding item in pricesheet (temporary).
				if (pricesheet[item]) {
					console.log("FOUND ITEM ON PRICESHEET");
					var price = pricesheet[item].price;
					//Checking item won't go over withdraw amount.
					console.log(price);
					console.log(currentTotal+price);
					console.log(amount);
					if(currentTotal+price <= amount) {
						console.log("ADDING TO TRADE");
						currentTotal+=price;
						console.log("TOTAL = "+currentTotal);
						//Adding item to trade.
						offer.addMyItem(myInventory[i]);
					}
				}
			}
		}

		//Checking if trade is good.
		if (goodOffer==true || currentTotal>=amount-outer.config.variance) {
			//Report to server that all is well.
			socket.emit('tradeSuccess', id);
			//Send trade.
			offer.send();
		} else {
			console.log("OFFER "+goodOffer);
			//Send trade.
			offer.send();
			//Report to server that further trade is needed from another bot.
			socket.emit('tradeUnfinished', {id: id, tradeurl: url, amount: amount-currentTotal});
		}
	});
 });

